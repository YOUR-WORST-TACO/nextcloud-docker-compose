# Nextcloud-Docker-Compose

## Starting
To run simply type:
```
docker-compose up -d
```

## Stopping
To stop simply type:
```
docker-compose down
```

## Default Config
on the first run it will take several minutes to setup the data folder,
when it is complete type:
```
cp config.php www/config
```