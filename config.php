<?php
$CONFIG = array (
  'htaccess.RewriteBase' => '/',
  'memcache.local' => '\\OC\\Memcache\\APCu',
  'apps_paths' => 
  array (
    0 => 
    array (
      'path' => '/var/www/html/apps',
      'url' => '/apps',
      'writable' => false,
    ),
    1 => 
    array (
      'path' => '/var/www/html/custom_apps',
      'url' => '/custom_apps',
      'writable' => true,
    ),
  ),
  'instanceid' => 'ocl7qzrg6e9j',
  'passwordsalt' => 'dWi2wdL7AQxmeriMjNDLAqp6SRq29n',
  'secret' => 'csVogPtl6wihrlKKshi4ywJYrwHz7UeDXuf8Dr98Hu+f06xz',
  'trusted_domains' => 
  array (
    0 => 'localhost:8080',
  ),
  'datadirectory' => '/var/www/html/data',
  'dbtype' => 'sqlite3',
  'version' => '15.0.5.3',
  'overwrite.cli.url' => 'http://localhost:8080',
  'installed' => true,
  'check_data_directory_permissions' => false,
  'debug' => true,
);
